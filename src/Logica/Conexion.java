package Logica;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class Conexion {
    //defino variables de conexion
    String user = "root";
    String pass = "";
    String url = "jdbc:mysql://localhost:3306/guiaestudio";
    
    
  public Conexion(){
      
  }
  
  public Connection conexion(){
      //defino avriable con
      Connection con = null;
      
      try {
          Class.forName("org.gjt.mm.mysql.Driver");
          con = DriverManager.getConnection(user, pass, url);
      } catch (ClassNotFoundException | SQLException e) {
          System.out.println(e);
      }
      
      return con;
          }
    
    
}
